#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import rospy
import cv2
import numpy as np
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import time

class Plantdetector():
    def __init__(self):
        # Subscriber to the image frame from camera_capture.py
        self.sub = rospy.Subscriber("/asc/plant_image", Image, self.load_image_callback)
        self.net = cv2.dnn.readNetFromDarknet('/home/asc15/team-i-hehe/catkin_ws/src/asclinic_pkg/src/nodes/yolo/yolov3tiny.cfg', '/home/asc15/team-i-hehe/catkin_ws/src/asclinic_pkg/src/nodes/yolo/yolov3-tiny.weights')
        self.net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)

        self.ln = self.net.getLayerNames()
        self.ln = [self.ln[i - 1] for i in self.net.getUnconnectedOutLayers()]


        self.classes = open('/home/asc15/team-i-hehe/catkin_ws/src/asclinic_pkg/src/nodes/yolo/coco.names').read().strip().split('\n') # Names of the classes
        np.random.seed(42)
        self.colors = np.random.randint(0, 255, size=(len(self.classes), 3), dtype='uint8') # Colours to use for the bounding boxes
        
        
  
    # Callback when subscriber recieves a frame
    def load_image_callback(self, msg: Image):

        # Convert it into into a numpy array to process the image:
        img = CvBridge().imgmsg_to_cv2(msg) # 1920x1080x3

        blob = cv2.dnn.blobFromImage(img, 1/255.0, (416, 416), swapRB=True, crop=False)
        self.net.setInput(blob)
        t0 = time.time()
        outputs = self.net.forward(self.ln)
        
        t = time.time() - t0
        outputs = np.vstack(outputs)

        self.post_process(img, outputs, 0.5)


        cv2.imwrite('/home/asc15/team-i-hehe/catkin_ws/src/asclinic_pkg/src/nodes/yolo/output.jpg', img)
        print(f"Image processed in {t}")


    def post_process(self, img, outputs, conf):
        H, W = img.shape[:2]

        boxes = []
        confidences = []
        classIDs = []

        for output in outputs:
            scores = output[5:]
            classID = np.argmax(scores)
            confidence = scores[classID]
            if confidence > conf: # threshold
                x, y, w, h = output[:4] * np.array([W, H, W, H])
                p0 = int(x - w//2), int(y - h//2)
                p1 = int(x + w//2), int(y + h//2)
                boxes.append([*p0, int(w), int(h)])
                confidences.append(float(confidence))
                classIDs.append(classID)
                # cv.rectangle(img, p0, p1, WHITE, 1)

        indices = cv2.dnn.NMSBoxes(boxes, confidences, conf, conf-0.1)
        if len(indices) > 0:
            for i in indices.flatten():
                
                # print(x,y,w,h,self.classes[classIDs[i]]) # Pixel coordinates of what object (+ confidence)

                (x, y) = (boxes[i][0], boxes[i][1])
                (w, h) = (boxes[i][2], boxes[i][3])
                color = [int(c) for c in self.colors[classIDs[i]]]


                cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
                text = "{}: {:.4f}".format(self.classes[classIDs[i]], confidences[i])
                cv2.putText(img, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 1)

    

if __name__ == '__main__':

    rospy.init_node('plant_detection2')

    plant_detection2 = Plantdetector()

    rospy.spin()