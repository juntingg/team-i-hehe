#! /usr/bin/env python3

import rospy
from sensor_msgs.msg import LaserScan

def LidarCallback(msg):
    dist = msg.ranges[180]
    rospy.loginfo("front: %f meters",dist)
    #rospy.loginfo(msg.ranges)

if __name__ == "__main__":
    rospy.init_node('lidar_node')
    lidar_sub = rospy.Subscriber('/asc/scan', LaserScan, LidarCallback)
    rospy.spin()
