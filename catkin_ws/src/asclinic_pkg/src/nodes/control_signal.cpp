
// Include some useful libraries
#include <cmath>
#include <stdlib.h>

// Include the ROS libraries
#include "ros/ros.h"
#include <ros/package.h>

// Include the standard message types
#include "std_msgs/Int32.h"
#include "std_msgs/Float32.h"
#include <std_msgs/String.h>
#include "asclinic_pkg/LeftRightInt32.h"
#include "asclinic_pkg/LeftRightFloat32.h"

#include <vector>

// Publisher variable:
ros::Publisher control_signal_publisher;

// Timer variable used for recurrent publishing:
ros::Timer m_timer_for_publishing;

// current pwm value
float pwm_target_left = 0;
float pwm_target_right = 0;
float pwm_current_left = 0;
float pwm_current_right = 0;
int i = 0;

double integral_left = 0.0;
double integral_right = 0.0;

int counts_left_A;
int counts_left_B;
int counts_right_A;
int counts_right_B;
int counts_left_sum = 0;
int counts_right_sum = 0;
int counts_left_delt = 0;
int counts_right_delt = 0;
float ave_count_delt_left = 0;
float ave_count_delt_right = 0;

float a_1 = -0.00688;
float a_2 = -0.00719;

float b_1 = 1.375;
float b_2 = 1.433;

float c_1 = -6.56;
float c_2 = -6.401;

// counts left & right call back
void CountsleftCallback(const asclinic_pkg::LeftRightInt32& msg)
{
	counts_left_A = msg.left;
	counts_left_B = msg.right;
	// ROS_INFO_STREAM("[Controller signal] counts left A:" << counts_left_A << " counts right B:" << counts_left_B);
	counts_left_delt = counts_left_A + counts_left_B - counts_left_sum;
	// ROS_INFO_STREAM("[Controller signal] counts delt left :" << counts_left_delt );
	counts_left_sum = counts_left_A + counts_left_B;
}

void CountsrightCallback(const asclinic_pkg::LeftRightInt32& msg)
{
	counts_right_A = msg.left;
	counts_right_B = msg.right;
	// ROS_INFO_STREAM("[Controller signal] counts right A:" << counts_right_A << " counts right B:" << counts_right_B);
	counts_right_delt = counts_right_A + counts_right_B - counts_right_sum;
	// ROS_INFO_STREAM("[Controller signal] counts delt right:" << counts_right_delt );
	counts_right_sum = counts_right_A + counts_right_B;
}

// PI controller function left
float PI_left(float target_speed, float current_speed, double integral)
{
	float error;
	error = target_speed - current_speed;
	const double Ts = 0.02; // Sampling time
	double Kp = 0.6; // Proportional gain
    double Ki = 0.7; // Integral gain
    integral += error * Ts;
    double u = Kp * error + Ki * integral;
    return u;
}

// PI controller right
float PI_right(float target_speed, float current_speed, double integral)
{
	float error;
	error = target_speed - current_speed;
	const double Ts = 0.02; // Sampling time
	double Kp = 0.609; // Proportional gain
    double Ki = 0.7; // Integral gain
    integral += error * Ts;
    double u = Kp * error + Ki * integral;
    return u;
}


// > Callback run when timer fires
void targetpwmDataCallback(const asclinic_pkg::LeftRightFloat32& msg)
{
	pwm_target_left = 2 * msg.left;
	pwm_target_right = 2 * msg.right;

	// ROS_INFO_STREAM("[Control signal] Message received target with left = " << msg.left << ", right = " << msg.right);

	if (!(pwm_target_left == 0 && pwm_target_right == 0))
	{
		pwm_current_left = (-b_1+sqrt(b_1*b_1 - 4*a_1*(c_1-counts_left_delt)))/(2*a_1);
		pwm_current_right = (-b_2+sqrt(b_2*b_2 - 4*a_2*(c_2-counts_right_delt)))/(2*a_2);

		i++;
		if (i>=40 && i<= 140)
		{
			ave_count_delt_left += counts_left_delt;
			ave_count_delt_right += counts_right_delt; 
		}

		// ROS_INFO_STREAM("[Control signal] Message received current with left = " << pwm_current_left << ", right = " << pwm_current_right << "  " << i);
		pwm_current_left = PI_left(pwm_target_left, pwm_current_left, integral_left);
		pwm_current_right = PI_right(pwm_target_right, pwm_current_right, integral_right);

	}

	if (pwm_target_left == 0 && pwm_target_right == 0)
	{
		pwm_current_left = pwm_target_left;
		pwm_current_right = pwm_target_right;
		// ROS_INFO_STREAM("[Control signal] average counts delt in 0.02s left = " << ave_count_delt_left/101 << ", right = " << ave_count_delt_right/101);
	}

	// ROS_INFO_STREAM("[Control Signal] control pwm left: " << pwm_current_left << " , pwm right: " << pwm_current_right);

	asclinic_pkg::LeftRightFloat32 control_speed_msg;
	control_speed_msg.left  = pwm_current_left;
	control_speed_msg.right = pwm_current_right;
	control_signal_publisher.publish(control_speed_msg);
} 

int main(int argc, char* argv[])
{

	ros::init(argc, argv, "control_signal");
	ros::NodeHandle nodeHandle("~");

	std::string ns_for_group = ros::this_node::getNamespace();
	ros::NodeHandle nh_for_group(ns_for_group);

	control_signal_publisher = nh_for_group.advertise<asclinic_pkg::LeftRightFloat32>("set_motor_duty_cycle", 2, false);

	ros::Subscriber target_pwm_subscriber = nh_for_group.subscribe("control_signal", 1, targetpwmDataCallback);
	ros::Subscriber counts_left_subscriber = nh_for_group.subscribe("counts_left", 1, CountsleftCallback);
	ros::Subscriber counts_right_subscriber = nh_for_group.subscribe("counts_right", 1, CountsrightCallback);

	ros::spin();

	return 0;
}
