
// Include some useful libraries
#include <cmath>
#include <stdlib.h>
#include <iostream>

// Include the ROS libraries
#include "ros/ros.h"
#include <ros/package.h>

// Include the standard message types
#include "std_msgs/Int32.h"
#include "std_msgs/Float32.h"
#include <std_msgs/String.h>
#include "asclinic_pkg/LeftRightInt32.h"
#include "asclinic_pkg/LeftRightFloat32.h"
#include "geometry_msgs/PoseStamped.h"

#include <vector>

// Publisher variable:
ros::Publisher odometry_publisher;

// Timer variable used for recurrent publishing:
ros::Timer m_timer_for_publishing;

float x_sum = 1.83, y_sum = 0.81, phi_sum = 0.0;

float alpha = 0.9;
float prevOutput_left_1 = 0.0;
float prevOutput_right_1 = 0.0;
float prevOutput_left_2 = 0.0;
float prevOutput_right_2 = 0.0;
float prevOutput_left_3 = 0.0;
float prevOutput_right_3 = 0.0;

float prevOutput_left = 0.0;
float prevOutput_right = 0.0;

int windowSize = 3;


std::vector<float> input_left = {0,0,0};
std::vector<float> input_right = {0,0,0};
std::vector<float> weights = {0.1, 0.1, 0.8};

int current_speed_left_flag;
int current_speed_right_flag;

float x_vision_pose = 0.0;
float y_vision_pose = 0.0;
float phi_vision_pose = 0.0;
bool vision_flag = 0;

float lowPassFilter(float prevOutput, float input) 
{
    float output = alpha * input + (1 - alpha) * prevOutput;
    return output;
}

// subscribe the current speed
void driveMotorsSubscriberCallback(const asclinic_pkg::LeftRightFloat32& msg) 
{
	if (msg.left >= 0)
		{current_speed_left_flag = 1;}
	else
		{current_speed_left_flag = -1;}

	if (msg.right >= 0)
		{current_speed_right_flag = 1;}
	else
		{current_speed_right_flag = -1;}

	// ROS_INFO_STREAM("[odometry] left flag : " << current_speed_left_flag << " right flag : " << current_speed_right_flag);
	// ROS_INFO_STREAM("[odometry] left : " << msg.left << " right : " << msg.right);
}

float wmaFilter(const std::vector<float>& input, const std::vector<float>& weights, int windowSize) 
{
    float sumWeights = 0.0;
    float sumValues = 0.0;

    for (int i = 0; i < windowSize; i++) {
        sumValues += weights[i] * input[i];
        sumWeights += weights[i];
    }

    return sumValues / sumWeights;
}

void visionposeDataCallback(const geometry_msgs::Pose& vision_pose)
{
	x_vision_pose = vision_pose.position.x;
	y_vision_pose = vision_pose.position.y;
	phi_vision_pose = vision_pose.orientation.w;
	// ROS_INFO_STREAM("[odometry] x vision pose:" << x_vision_pose << "y vision pose:" << y_vision_pose << "vision theta:" << phi_vision_pose );
	if ((x_vision_pose != 0) && (y_vision_pose != 0) && (phi_vision_pose != 0))
	{vision_flag = 1;}
}

// > Callback run when timer fires
void encodercountsDataCallback(const asclinic_pkg::LeftRightInt32& msg)
{
	float dt = 0.02;
	float left_theta;
	float right_theta;

	float det_x;
	float det_y;
	float det_phi;

	left_theta = current_speed_left_flag * msg.left *2*M_PI/4500;
	right_theta = current_speed_right_flag * msg.right *2*M_PI/4350;

	// left_theta =  msg.left *2*M_PI/4480;
	// right_theta =  msg.right *2*M_PI/4480;

	// wmaFilter
	prevOutput_left_1 = prevOutput_left_2;
	prevOutput_left_2 = prevOutput_left_3;
	prevOutput_left_3 = left_theta;

	prevOutput_right_1 = prevOutput_right_2;
	prevOutput_right_2 = prevOutput_right_3;
	prevOutput_right_3 = right_theta;

	input_left = {prevOutput_left_1,prevOutput_left_2,prevOutput_left_3};
	input_right = {prevOutput_right_1,prevOutput_right_2,prevOutput_right_3};

    float filteredValue_left = wmaFilter(input_left, weights, windowSize);
	float filteredValue_right = wmaFilter(input_right, weights, windowSize);

	left_theta = filteredValue_left;
	right_theta = filteredValue_right;
	// ROS_INFO_STREAM("[odometry] left theta : " << left_theta << " right theta : " << right_theta);
	
	//simple lowpass filter
	// left_theta = lowPassFilter(prevOutput_left, left_theta);
	// right_theta = lowPassFilter(prevOutput_right, right_theta);

	// prevOutput_left = left_theta; 
	// prevOutput_right = right_theta;

	det_x = 1.98 * (0.07/2)*(left_theta + right_theta) * cos(phi_sum + 0.5 * (right_theta - left_theta) * (0.07/0.215));
	det_y = 1.63 * (0.07/2)*(left_theta + right_theta) * sin(phi_sum + 0.5 * (right_theta - left_theta) * (0.07/0.215));
	det_phi = 2.105 * (right_theta - left_theta) * (0.07/0.215);

	x_sum += det_x;
	y_sum += det_y;
	phi_sum += det_phi;

	if (vision_flag != 0)
	{	
		x_sum = x_vision_pose;
		y_sum = y_vision_pose;
		phi_sum = phi_vision_pose;
		vision_flag = 0;
		ROS_INFO_STREAM("[odometry] x vision pose:" << x_sum << "y pose:" << y_sum << "theta:" << phi_sum  );
	}

	geometry_msgs::Pose pose_odometry;
    pose_odometry.position.x = x_sum;
	pose_odometry.position.y = y_sum;
	pose_odometry.orientation.w = phi_sum * 180/M_PI;
	odometry_publisher.publish(pose_odometry);

	// print current pose in odometry 
	// ROS_INFO_STREAM("[odometry] x pose:" << x_sum << "y pose:" << y_sum << "theta:" << phi_sum * 180 / M_PI);
	
}

int main(int argc, char* argv[])
{

	ros::init(argc, argv, "odometry_node");
	ros::NodeHandle nodeHandle("~");

	std::string ns_for_group = ros::this_node::getNamespace();
	ros::NodeHandle nh_for_group(ns_for_group);

	odometry_publisher = nh_for_group.advertise<geometry_msgs::Pose>("odometry", 10, false);

	ros::Subscriber encoder_counters_subscriber = nh_for_group.subscribe("encoder_counts", 1, encodercountsDataCallback);

	ros::Subscriber current_motor_duty_cycle_subscriber = nh_for_group.subscribe("current_motor_duty_cycle", 2, driveMotorsSubscriberCallback);

	ros::Subscriber vision_location_subscriber = nh_for_group.subscribe("pose_topic", 1, visionposeDataCallback);

	ros::spin();

	return 0;
}
