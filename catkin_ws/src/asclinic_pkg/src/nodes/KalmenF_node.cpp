#include <ros/ros.h>
#include <Eigen/Dense>
#include <geometry_msgs/Vector3.h>
#include <sensor_msgs/Imu.h>

using namespace Eigen;

class KalmanFilter {
public:
    KalmanFilter() {
        // Initialize state and covariance matrices
        x_ = Vector3d::Zero(); // initial state is zero
        P_ = Matrix3d::Identity(); // initial covariance matrix is identity
    }

    // Predict function: predict the state and covariance matrices based on the motion model
    void predict(const double dt) {
        // Define the motion model
        Matrix3d F;
        F << 1, dt, 0,
             0, 1, 0,
             0, 0, 1;
        Matrix3d Q;
        Q << 0.1, 0, 0,
             0, 0.1, 0,
             0, 0, 0.1;

        // Predict the state and covariance matrices
        x_ = F * x_;
        P_ = F * P_ * F.transpose() + Q;
    }

    // Update function: update the state and covariance matrices based on a measurement
    void update(const Vector3d& z) {
        // Define the measurement model
        Matrix3d H;
        H << 1, 0, 0,
             0, 1, 0,
             0, 0, 1;
        Matrix3d R;
        R << 0.1, 0, 0,
             0, 0.1, 0,
             0, 0, 0.1;

        // Calculate the Kalman gain
        Matrix3d K = P_ * H.transpose() * (H * P_ * H.transpose() + R).inverse();

        // Update the state and covariance matrices
        x_ = x_ + K * (z - H * x_);
        P_ = (Matrix3d::Identity() - K * H) * P_;
    }

private:
    Vector3d x_; // state vector
    Matrix3d P_; // covariance matrix
};

int main(int argc, char** argv) {
    ros::init(argc, argv, "kalman_filter");
    ros::NodeHandle nh;

    // // Subscribe odometry and vision localization topic for measurements
    // // odometry (odometry pose)
    // // pose_topic (vision pose)
    // ros::Subscriber localization_odometry_subscriber = nh.subscribe("odometry", 10, &KalmanFilter::update, &kf);
    // ros::Subscriber localization_vision_subscriber = nh.subscribe("pose_topic", 10, );

    // Set up loop rate
    ros::Rate loop_rate(50); // 50 Hz

    // Initialize Kalman filter
    KalmanFilter kf;

    while (ros::ok()) {
        // Predict the state and covariance matrices based on the motion model
        kf.predict(dt);

        // Update the state and covariance matrices based on the latest measurement
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}
