

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <linux/joystick.h>
#include <sys/ioctl.h>

#include <cmath>
#include <stdlib.h>

// Include the ROS libraries
#include "ros/ros.h"
#include <ros/package.h>

// Include the standard message types
#include "std_msgs/Int32.h"
#include "std_msgs/Float32.h"
#include <std_msgs/String.h>
#include "asclinic_pkg/LeftRightInt32.h"
#include "asclinic_pkg/LeftRightFloat32.h"
#include "geometry_msgs/PoseStamped.h"

/**
 * Reads a joystick event from the joystick device.
 *
 * Returns 0 on success. Otherwise -1 is returned.
 */

ros::Publisher joystick_control_publisher;
ros::Publisher joystick_other_control_publisher;
ros::Publisher joystick_servo_control_publisher;
ros::Publisher m_timer_for_publishing;

float m_delta_t_for_publishing_counts = 0.02;

void timerCallbackForPublishing(const ros::TimerEvent&)
{

}

int read_event(int fd, struct js_event *event)
{
    ssize_t bytes;

    bytes = read(fd, event, sizeof(*event));

    if (bytes == sizeof(*event))
        return 0;

    /* Error, could not read full event. */
    return -1;
}

/**
 * Returns the number of axes on the controller or 0 if an error occurs.
 */
size_t get_axis_count(int fd)
{
    __u8 axes;

    if (ioctl(fd, JSIOCGAXES, &axes) == -1)
        return 0;

    return axes;
}

/**
 * Returns the number of buttons on the controller or 0 if an error occurs.
 */
size_t get_button_count(int fd)
{
    __u8 buttons;
    if (ioctl(fd, JSIOCGBUTTONS, &buttons) == -1)
        return 0;

    return buttons;
}

/**
 * Current state of an axis.
 */
struct axis_state {
    short x, y;
};

/**
 * Keeps track of the current axis state.
 *
 * NOTE: This function assumes that axes are numbered starting from 0, and that
 * the X axis is an even number, and the Y axis is an odd number. However, this
 * is usually a safe assumption.
 *
 * Returns the axis that the event indicated.
 */
size_t get_axis_state(struct js_event *event, struct axis_state axes[3])
{
    size_t axis = event->number / 2;

    if (axis < 3)
    {
        if (event->number % 2 == 0)
            axes[axis].x = event->value;
        else
            axes[axis].y = event->value;
    }

    return axis;
}

int main(int argc, char *argv[])
{   
    ros::init(argc, argv, "joystick_control");
    ros::NodeHandle nodeHandle("~");

    std::string ns_for_group = ros::this_node::getNamespace();
    ros::NodeHandle nh_for_group(ns_for_group);

    const char *device;
    int js;
    struct js_event event;
    struct axis_state axes[3] = {0};
    size_t axis;

    if (argc > 1)
        device = argv[1];
    else
        device = "/dev/input/js0";

    js = open(device, O_RDONLY);

    if (js == -1)
    {
        perror("Could not open joystick");
        return 1;
    }

    joystick_control_publisher = nh_for_group.advertise<asclinic_pkg::LeftRightFloat32>("joy_control", 2, false);
    joystick_other_control_publisher = nh_for_group.advertise<asclinic_pkg::LeftRightInt32>("joy_other_control", 2, false);
    joystick_servo_control_publisher = nh_for_group.advertise<asclinic_pkg::LeftRightFloat32>("joy_servo_control", 2, false);

    // m_timer_for_publishing = nodeHandle.createTimer(ros::Duration(m_delta_t_for_publishing_counts), timerCallbackForPublishing, false);

    /* This loop will exit if the controller is unplugged. */
    while (read_event(js, &event) == 0)
    {   
        asclinic_pkg::LeftRightFloat32 target_msg;
        asclinic_pkg::LeftRightFloat32 servo_msg;
        asclinic_pkg::LeftRightInt32 button_msg;
        switch (event.type)
        {   
            case JS_EVENT_BUTTON:
                // printf("Button %u %s\n", event.number, event.value ? "pressed" : "released");
                button_msg.left = event.number;
                button_msg.right = event.value;
                joystick_other_control_publisher.publish(button_msg);
                break;
            case JS_EVENT_AXIS:
                axis = get_axis_state(&event, axes);
                if (axis < 3)
                    // printf("Axis %zu at (%6d, %6d)\n", axis, axes[axis].x, axes[axis].y);
                    
                    target_msg.left  = axes[0].x;
                    target_msg.right = axes[0].y;
                    servo_msg.left = axes[1].y;
                    servo_msg.right = axes[2].x;
                    joystick_control_publisher.publish(target_msg);
                    joystick_servo_control_publisher.publish(servo_msg);
                    // printf("Axis %zu at (%6d, %6d)\n", axis, axes[axis].x, axes[axis].y);
                break;
            
        }

        fflush(stdout);
    }

    close(js);
    ros::spin();
    return 0;
}

