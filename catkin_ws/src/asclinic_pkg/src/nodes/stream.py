#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import cv2

# 初始化ROS节点
rospy.init_node('opencv_camera_to_ros_video')

# 创建图像发布者
image_publisher = rospy.Publisher('image_topic', Image, queue_size=10)

# 创建CvBridge实例
bridge = CvBridge()

# 打开OpenCV摄像头
video_capture = cv2.VideoCapture(USB_CAMERA_DEVICE_NUMBER)  # 0表示默认摄像头

# 获取摄像头的帧率
frame_rate = video_capture.get(cv2.CAP_PROP_FPS)

# 循环读取摄像头帧并发布ROS图像消息
rate = rospy.Rate(frame_rate)  # 设置发布频率
while not rospy.is_shutdown():
    ret, frame = video_capture.read()

    if ret:
        # 转换为ROS图像消息
        ros_image = bridge.cv2_to_imgmsg(frame, encoding="bgr8")

        # 发布ROS图像消息
        image_publisher.publish(ros_image)

        rate.sleep()

# 释放摄像头资源
video_capture.release()
