#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Import the ROS-Python package
import rospy
import math
#import rospkg
import time
# Import the standard message types
from std_msgs.msg import UInt32
from sensor_msgs.msg import Image
from geometry_msgs.msg import Pose,Point, Quaternion
import numpy as np
# Import opencv
import cv2
# Import aruco
import cv2.aruco as aruco
# Package to convert between ROS and OpenCV Images
from cv_bridge import CvBridge, CvBridgeError



# DEFINE THE PARAMETERS
# > For the number of the USB camera device
#   i.e., for /dev/video0, this parameter should be 0
USB_CAMERA_DEVICE_NUMBER = 0

# > Properties of the camera images captured
DESIRED_CAMERA_FRAME_HEIGHT = 1080
DESIRED_CAMERA_FRAME_WIDTH = 1920
DESIRED_CAMERA_FPS = 5

MARKER_SIZE = 0.250
# > For where to save images captured by the camera
#   Note: ensure that this path already exists
#   Note: images are only saved when a message is received
#         on the "request_save_image" topic.
SAVE_IMAGE_PATH = "/home/asc15/plant_detection/"

# > A flag for whether to save any images that contains
#   a camera calibration PLANT
SHOULD_SAVE_PLANT_IMAGES = True

# > A flag for whether to display the images captured
SHOULD_SHOW_IMAGES = False
plant_found = False
SAVE_IMAGE_PATH2 = "/home/asc15/saved_ArUco_images/"

# > A flag for whether to display the images captured
SHOULD_SHOW_IMAGES2 = False

# > A flag for whether to publish the images captured
SHOULD_PUBLISH_CAMERA_IMAGES2 = False

class CameraCapture:

    def __init__(self):
        self.counter = 0
        self.counter_aruco = 0
        
        # Initialise a publisher for the images
        self.image_publisher = rospy.Publisher("/asc"+"/plant_image", Image, queue_size=10)
        self.pub = rospy.Publisher('pose_topic',Pose,queue_size = 10)
        # Initialise a subscriber for flagging when to save an image
        rospy.Subscriber("/asc"+"/request_plant_image", UInt32, self.requestSaveImageSubscriberCallback)
        self.image_publisher2 = rospy.Publisher("/asc"+"/camera_image", Image, queue_size=10)
        self.image_publisher3= rospy.Publisher("/asc"+"/image_topic", Image, queue_size=10)
        # Initialise a subscriber for flagging when to save an image
        rospy.Subscriber("/asc"+"/request_save_image", UInt32, self.requestSaveImageSubscriberCallback)
        # > For convenience, the command line can be used to trigger this subscriber
        #   by publishing a message to the "request_save_image" as follows:
        #
        # rostopic pub /asc/request_save_image std_msgs/UInt32 "data: 1" 

        # Initialise variables for managing the saving of an image
        self.save_image_counter = 0
        self.save_image_counter2 = 0
        self.kernel = np.array([[-1, -1, -1],
                   [-1,  9, -1],
                   [-1, -1, -1]])
        self.should_save_image = False
        self.should_save_image2 = False
        # Specify the details for camera to capture from

        # > Put the desired video capture properties into local variables
        self.camera_frame_width  = DESIRED_CAMERA_FRAME_WIDTH
        self.camera_frame_height = DESIRED_CAMERA_FRAME_HEIGHT
        self.camera_fps = DESIRED_CAMERA_FPS
        
        # > For capturing from a USB camera:
        #   > List the contents of /dev/video* to determine
        #     the number of the USB camera
        #   > If "v4l2-ctl" command line tool is installed then list video devices with:
        #     v4l2-ctl --list-devices
        self.camera_setup = USB_CAMERA_DEVICE_NUMBER
        
                # > For capture from a camera connected via the MIPI CSI cable connectors
        #   > This specifies the gstreamer pipeline for video capture
        #   > sensor-id=0 for CAM0 and sensor-id=1 for CAM1
        #   > This should work; it is not "optimized"; precise details depend on the camera connected
        #self.camera_setup = 'nvarguscamerasrc sensor-id=0 ! video/x-raw(memory:NVMM), width=1920, height=1080, framerate=12/1, format=MJPG ! nvvidconv flip-method=0 ! video/x-raw, width = 800, height=600, format =BGRx ! videoconvert ! video/x-raw, format=BGR ! appsink'

        # Initialise video capture from the camera
        self.cam=cv2.VideoCapture(self.camera_setup)

        # Display the properties of the camera upon initialisation
        # > A list of all the properties available can be found here:
        #   https://docs.opencv.org/4.x/d4/d15/group__videoio__flags__base.html#gaeb8dd9c89c10a5c63c139bf7c4f5704d
        print("\n[CAMERA CAPTURE] Camera properties upon initialisation:")
        print("CV_CAP_PROP_FRAME_HEIGHT : '{}'".format(self.cam.get(cv2.CAP_PROP_FRAME_HEIGHT)))
        print("CV_CAP_PROP_FRAME_WIDTH :  '{}'".format(self.cam.get(cv2.CAP_PROP_FRAME_WIDTH)))
        print("CAP_PROP_FPS :             '{}'".format(self.cam.get(cv2.CAP_PROP_FPS)))
        print("CAP_PROP_FOCUS :           '{}'".format(self.cam.get(cv2.CAP_PROP_FOCUS)))
        print("CAP_PROP_AUTOFOCUS :       '{}'".format(self.cam.get(cv2.CAP_PROP_AUTOFOCUS)))
        print("CAP_PROP_BRIGHTNESS :      '{}'".format(self.cam.get(cv2.CAP_PROP_BRIGHTNESS)))
        print("CAP_PROP_CONTRAST :        '{}'".format(self.cam.get(cv2.CAP_PROP_CONTRAST)))
        print("CAP_PROP_SATURATION :      '{}'".format(self.cam.get(cv2.CAP_PROP_SATURATION)))
        #print("CAP_PROP_HUE :             '{}'".format(self.cam.get(cv2.CAP_PROP_HUE)))
        #print("CAP_PROP_CONVERT_RGB :     '{}'".format(self.cam.get(cv2.CAP_PROP_CONVERT_RGB)))
        #print("CAP_PROP_POS_MSEC :        '{}'".format(self.cam.get(cv2.CAP_PROP_POS_MSEC)))
        #print("CAP_PROP_FRAME_COUNT  :    '{}'".format(self.cam.get(cv2.CAP_PROP_FRAME_COUNT)))
        print("CAP_PROP_BUFFERSIZE :      '{}'".format(self.cam.get(cv2.CAP_PROP_BUFFERSIZE)))

        # Set the camera properties to the desired values
        # > Frame height and  width, in [pixels]
        self.cam.set(cv2.CAP_PROP_FRAME_HEIGHT, self.camera_frame_height)
        self.cam.set(cv2.CAP_PROP_FRAME_WIDTH,  self.camera_frame_width)
        # > Frame rate, in [fps]
        self.cam.set(cv2.CAP_PROP_FPS, self.camera_fps)
        # > Auto focus, [bool: 0=off, 1=on]
        self.cam.set(cv2.CAP_PROP_AUTOFOCUS, 0)
        # > Focus absolute, [int: min=0 max=250 step=5 default=0 value=0 flags=inactive]
        #   0 corresponds to focus at infinity
        self.cam.set(cv2.CAP_PROP_FOCUS, 0)
        # > Buffer size, [int: min=1]
        #   Setting the buffer to zero ensures that we get that
        #   most recent frame even when the "timerCallbackForCameraRead"
        #   function takes longer than (1.self.camera_fps) seconds
        self.cam.set(cv2.CAP_PROP_BUFFERSIZE, 1)

        # Display the properties of the camera after setting the desired values
        print("\n[CAMERA CAPTURE] Camera properties upon initialisation:")
        print("CV_CAP_PROP_FRAME_HEIGHT : '{}'".format(self.cam.get(cv2.CAP_PROP_FRAME_HEIGHT)))
        print("CV_CAP_PROP_FRAME_WIDTH :  '{}'".format(self.cam.get(cv2.CAP_PROP_FRAME_WIDTH)))
        print("CAP_PROP_FPS :             '{}'".format(self.cam.get(cv2.CAP_PROP_FPS)))
        print("CAP_PROP_FOCUS :           '{}'".format(self.cam.get(cv2.CAP_PROP_FOCUS)))
        print("CAP_PROP_AUTOFOCUS :       '{}'".format(self.cam.get(cv2.CAP_PROP_AUTOFOCUS)))
        print("CAP_PROP_BRIGHTNESS :      '{}'".format(self.cam.get(cv2.CAP_PROP_BRIGHTNESS)))
        print("CAP_PROP_CONTRAST :        '{}'".format(self.cam.get(cv2.CAP_PROP_CONTRAST)))
        print("CAP_PROP_SATURATION :      '{}'".format(self.cam.get(cv2.CAP_PROP_SATURATION)))
        #print("CAP_PROP_HUE :             '{}'".format(self.cam.get(cv2.CAP_PROP_HUE)))
        #print("CAP_PROP_CONVERT_RGB :     '{}'".format(self.cam.get(cv2.CAP_PROP_CONVERT_RGB)))
        #print("CAP_PROP_POS_MSEC :        '{}'".format(self.cam.get(cv2.CAP_PROP_POS_MSEC)))
        #print("CAP_PROP_FRAME_COUNT  :    '{}'".format(self.cam.get(cv2.CAP_PROP_FRAME_COUNT)))
        print("CAP_PROP_BUFFERSIZE :      '{}'".format(self.cam.get(cv2.CAP_PROP_BUFFERSIZE)))

        # The frame per second (fps) property cannot take any value,
        # hence compare the actural value and display any discrepancy
        camera_actual_fps = self.cam.get(cv2.CAP_PROP_FPS)
        if not(camera_actual_fps==self.camera_fps):
            rospy.logwarn("[CAMERA CAPTURE] The camera is running at " + str(camera_actual_fps) + " fps, even though " + str(self.camera_fps) + " fps was requested.")
            rospy.logwarn("[CAMERA CAPTURE] The fps discrepancy is normal behaviour as most cameras cannot run at arbitrary fps rates.")
            rospy.logwarn("[CAMERA CAPTURE] Due to the fps discrepancy, updated the value: self.camera_fps = " + str(camera_actual_fps))
            self.camera_fps = camera_actual_fps
        self.intrinic_camera_matrix = np.array( [[1.37626202e+03,0,9.52013976e+02] , [0,1.3788940e+03,5.28395929e+02] , [0,0,1]], dtype=float)
        self.intrinic_camera_distortion  = np.array( [[ 4.80591842e-02, -4.15569119e-01, -2.72054253e-03, -2.80307412e-04, 9.47131580e-01]], dtype=float)
        # Initialise the OpenCV<->ROS bridge
        self.cv_bridge = CvBridge()
        # Get the ArUco dictionary to use
        self.aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_4X4_50)

        # Create an parameter structure needed for the ArUco detection
        self.aruco_parameters = aruco.DetectorParameters()
        # > Specify the parameter for: corner refinement
        self.aruco_parameters.cornerRefinementMethod = aruco.CORNER_REFINE_SUBPIX

        # Create an Aruco detector object
        self.aruco_detector = aruco.ArucoDetector(self.aruco_dict, self.aruco_parameters)
        # Read the a camera frame as a double check of the properties
        # > Read the frame
        marker_size_half = 0.5 * MARKER_SIZE
        self.single_marker_object_points = np.array([  \
                [-marker_size_half, marker_size_half, 0.0], \
                [ marker_size_half, marker_size_half, 0.0], \
                [ marker_size_half,-marker_size_half, 0.0], \
                [-marker_size_half,-marker_size_half, 0.0]  \
                ], dtype=np.float32 )
        self.intrinic_camera_matrix = np.array( [[1.37626202e+03,0,9.52013976e+02] , [0,1.3788940e+03,5.28395929e+02] , [0,0,1]], dtype=float)
        self.intrinic_camera_distortion  = np.array( [[ 4.80591842e-02, -4.15569119e-01, -2.72054253e-03, -2.80307412e-04, 9.47131580e-01]], dtype=float)
        return_flag , current_frame = self.cam.read()
        # > Get the dimensions of the frame
        dimensions = current_frame.shape
        # > Display the dimensions
        rospy.loginfo("[CAMERA CAPTURE] As a double check of the camera properties set, a frame captured just now has dimensions = " + str(dimensions))
        # > Also check the values
        if (not(dimensions[0]==self.camera_frame_height) or not(dimensions[1]==self.camera_frame_width)):
            rospy.logwarn("[CAMERA CAPTURE] ERROR: frame dimensions do NOT match the desired values.")
            # Update the variables
            self.camera_frame_height = dimensions[0]
            self.camera_frame_width  = dimensions[1]

        # Display the status
        rospy.loginfo("[CAMERA CAPTURE] Initialisation complete")

        # Initialise a timer for capturing the camera frames
        rospy.Timer(rospy.Duration(1/self.camera_fps), self.timerCallbackForCameraRead)
        # Publish the camera frame



    # Respond to timer callback
    def timerCallbackForCameraRead(self, event):
        # Read the camera frame
        #rospy.loginfo("[CAMERA CAPTURE] Now reading camera frame")
        return_flag , current_frame = self.cam.read()
        try:
            self.image_publisher3.publish(self.cv_bridge.cv2_to_imgmsg(current_frame, "bgr8"))
        except CvBridgeError as cv_bridge_err:
            print(cv_bridge_err)
        
        # Note: return_flag is false if no frame was grabbed

        # get dimensions of image
        #dimensions = current_frame.shape
        #height = current_frame.shape[0]
        #width = current_frame.shape[1]
        #channels = current_frame.shape[2]

        # Check if the camera frame was successfully read
        if (return_flag == True):
            # Publish the camera frame
            #rospy.loginfo("[CAMERA CAPTURE] detecting")
            self.image_publisher.publish(self.cv_bridge.cv2_to_imgmsg(current_frame))

            if (SHOULD_SAVE_PLANT_IMAGES):
                
                self.counter = self.counter + 1
                self.counter_aruco = self.counter_aruco + 1
                plant_found = False
                hsv = cv2.cvtColor(current_frame, cv2.COLOR_BGR2HSV)
                lower_green = (35,43,46)
                upper_green = (77,255,255)
                # Check if the camera frame contains a calibration
                # chessboard that can be detected by OpenCV
                # > Convert the image to gray scale
                mask = cv2.inRange(hsv, lower_green, upper_green)
                # only keep green part
                result = cv2.bitwise_and(current_frame, current_frame, mask=mask)
                gray=cv2.cvtColor(result,cv2.COLOR_BGR2GRAY)
                gray1=gray
                gray1[gray1>200]=255
                gray1[gray1<50]=0
                blur = cv2.medianBlur(gray1, 3)  # median blur
                retval, binary = cv2.threshold(blur, 55, 255, cv2.THRESH_BINARY) # 与环境有关, can be adjusted 前值越小 越能放噪音 但太小会导致本身轮廓消失
                kernel = np.ones((50, 50), np.uint8)
                obj_bin = cv2.erode(binary, kernel, iterations=1)
                kernel = np.ones((100, 100), np.uint8)
                obj_bin = cv2.dilate(obj_bin, kernel, iterations=1)
                AREA_THRESHOLD_L = 20000  #25000
                AREA_THRESHOLD_H = 70000  #100000    define the contour area
                canvas = np.copy(current_frame)
                obj_bin = np.array(obj_bin)
                contours, hier= cv2.findContours(obj_bin, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
                cv2.drawContours(canvas, contours, -1, (255, 0, 0), 3)    # for object contour
                #h1,w1 = template.shape[:2]
                for i, cnt in enumerate(contours):
                    x, y, w, h = cv2.boundingRect(cnt) # 大框 并非最小矩阵框
                    center_plant = np.array([x +0.5 * w,y + 0.5 * h])
                    center_image = np.array([1920// 2, 1080 // 2])
                    distance_plant = np.linalg.norm(center_image - center_plant)
                    #print("area:",w*h)
                    if w * h < AREA_THRESHOLD_L:
                        continue
                    elif w * h > AREA_THRESHOLD_H:
                        continue
                    elif w > 1000 or h > 1000:
                        continue
                    elif distance_plant >= 400:
                        continue
                    else:
                        plant_found = True
                find_flags = cv2.CALIB_CB_ADAPTIVE_THRESH #+ cv2.CALIB_CB_NORMALIZE_IMAGE + cv2.CALIB_CB_FAST_CHECK
                # > Call the "find chessboard corners" function:
                #   > The second argument is the grid size of internal
                #     corner points that the function should search for
                #   > This function can be quite slow when a chessboard is not visible in the image
                # If found, then set the save image flag to true
                if plant_found == True and self.counter >= 15:
                    self.counter = 0
                    rospy.loginfo("[CAMERA CAPTURE] plant FOUND, this image will be saved")
                    self.should_save_image = True
                    sharp_frame = cv2.filter2D(current_frame,-1,self.kernel)

            # Save the camera frame if requested
            if (self.should_save_image):
                # Increment the image counter
                self.save_image_counter += 1
                # Write the image to file
                temp_filename = SAVE_IMAGE_PATH + "image" + str(self.save_image_counter) + ".jpg"
                cv2.imwrite(temp_filename,sharp_frame)
                # Display the path to where the image was saved
                rospy.loginfo("[CAMERA CAPTURE] Saved camera frame to: " + temp_filename)
                # Reset the flag to false
                self.should_save_image = False
                #time.sleep(3)

            # Display the camera frame
            if (SHOULD_SHOW_IMAGES):
                #rospy.loginfo("[CAMERA CAPTURE] Now displaying camera frame")
                cv2.namedWindow('canvas', cv2.WINDOW_NORMAL)
                cv2.imshow("[CAMERA CAPTURE]", canvas)
        # Detect ArUco markers from the frame
            current_frame_gray = cv2.cvtColor(current_frame, cv2.COLOR_BGR2GRAY)
            aruco_corners_of_all_markers, aruco_ids, aruco_rejected_img_points = self.aruco_detector.detectMarkers(current_frame_gray)
            image_width = current_frame_gray.shape[1]
            image_height = current_frame_gray.shape[0]
            camera_center = (image_width // 2, image_height // 2)

            # 初始化变量
            closest_marker_index = -1  # 最近的标记索引
            closest_marker_distance = float('inf')  # 最近的标记距离

            # 遍历所有的标记
            for i, corners in enumerate(aruco_corners_of_all_markers):
                # 计算标记中心点坐标
                marker_center = np.mean(corners, axis=0)

                # 计算标记中心点与相机中心点之间的距离
                distance = np.linalg.norm(marker_center - camera_center)

                # 更新最近的标记索引和距离
                if distance < closest_marker_distance:
                    closest_marker_distance = distance
                    closest_marker_index = i

            # 检查是否找到最近的标记
            if closest_marker_index != -1:
                closest_marker_corners = aruco_corners_of_all_markers[closest_marker_index]
                closest_marker_id = aruco_ids[closest_marker_index]
                # Process any ArUco markers that were detected
            # Process any ArUco markers that were detected
            if aruco_ids is not None and self.counter_aruco >= 5:
                # Display the number of markers found
                if (len(aruco_ids)==1):
                    rospy.loginfo("[ARUCO DETECTOR] Found " + "{:3}".format(len(aruco_ids)) + " marker  with id  = " + str(aruco_ids[:,0]))
                else:
                    rospy.loginfo("[ARUCO DETECTOR] Found " + "{:3}".format(len(aruco_ids)) + " markers with ids = " + str(aruco_ids[:,0]))
                # Outline all of the markers detected found in the image
                current_frame_with_marker_outlines = aruco.drawDetectedMarkers(current_frame.copy(), aruco_corners_of_all_markers, aruco_ids, borderColor=(0, 220, 0))

                # Iterate over the markers detected
                if closest_marker_id is not None:
                    # Get the ID for this marker
                    this_id = aruco_ids[closest_marker_index]
                    # Get the corners for this marker
                    corners_of_this_marker = np.asarray(aruco_corners_of_all_markers[closest_marker_index][0], dtype=np.float32)
                    solvepnp_method = cv2.SOLVEPNP_IPPE_SQUARE
                    success_flag, rvec, tvec = cv2.solvePnP(self.single_marker_object_points, corners_of_this_marker, self.intrinic_camera_matrix, self.intrinic_camera_distortion, flags=solvepnp_method)

                    current_frame_with_marker_outlines = cv2.drawFrameAxes(current_frame_with_marker_outlines, self.intrinic_camera_matrix, self.intrinic_camera_distortion, rvec, tvec, 0.5*MARKER_SIZE) 

                    Rmat = cv2.Rodrigues(rvec)

                    rospy.loginfo("[ARUCO DETECTOR] for id = " + str(this_id) + ", tvec = [ " + str(tvec[0]) + " , " + str(tvec[1]) + " , " + str(tvec[2]) + " ]" )
                    # ADD YOUR CODE HERE
                    if str(closest_marker_id) == "[4]": #RMI
                        pitch = np.deg2rad(0) #y
                        yaw = np.deg2rad(90) #z
                        roll = np.deg2rad(-90) #x
                        TOM = np.array([[6.5],[3.5],[0.275]]) #marker absolute position for world
                    elif str(closest_marker_id) == "[12]": #RMI
                        pitch = np.deg2rad(0) #y
                        yaw = np.deg2rad(90) #z
                        roll = np.deg2rad(-90) #x
                        TOM = np.array([[6.03],[2.82],[0.275]]) #marker absolute position for world
                    elif str(closest_marker_id) == "[8]":
                        pitch = np.deg2rad(0) #y
                        yaw = np.deg2rad(-180) #z
                        roll = np.deg2rad(-90) #x
                        TOM = np.array([[3],[7],[0.275]])
                    elif str(closest_marker_id) == "[7]":
                        pitch = np.deg2rad(0) #y
                        yaw = np.deg2rad(-180) #z
                        roll = np.deg2rad(-90) #x
                        TOM = np.array([[4],[5],[0.275]])
                    elif str(closest_marker_id) == "[22]":
                        pitch = np.deg2rad(0) #y
                        yaw = np.deg2rad(-180) #z
                        roll = np.deg2rad(-90) #x
                        TOM = np.array([[5.13],[4.46],[0.275]])
                    elif str(closest_marker_id) == "[18]":
                        pitch = np.deg2rad(0) #y
                        yaw = np.deg2rad(-90) #z
                        roll = np.deg2rad(-90) #x
                        TOM = np.array([[2],[4.5],[0.275]])
                    elif str(closest_marker_id) == "[9]": #RMI
                        pitch = np.deg2rad(0) #y
                        yaw = np.deg2rad(90) #z
                        roll = np.deg2rad(-90) #x
                        TOM = np.array([[6.5],[7.5],[0.275]])
                    else:
                        pitch = np.deg2rad(0) #y
                        yaw = np.deg2rad(90) #z
                        roll = np.deg2rad(-90) #x
                        TOM = np.array([[6.5],[7.5],[0.275]])
                    cy = np.cos(yaw)
                    sy = np.sin(yaw)
                    cp = np.cos(pitch)
                    sp = np.sin(pitch)
                    cr = np.cos(roll)
                    sr = np.sin(roll)

                    Rz = np.array([[cy, -sy, 0], [sy, cy, 0], [0, 0, 1]])
                    Ry = np.array([[cp, 0, sp], [0, 1, 0], [-sp, 0, cp]])
                    Rx = np.array([[1, 0, 0], [0, cr, -sr], [0, sr, cr]])
                    # return rotation matrix
                    RMI= Rz @ Ry @ Rx
                    RCM = np.linalg.inv(Rmat[0])
                    #TOM = np.array([[-0.53],[1.32],[0.32]])
                    locationxyz = TOM - RMI @ RCM @ tvec 
                    RBI = RMI @ RCM
                    angle = np.arctan2(RBI[1][0],RBI[0][0]) + (math.pi)/2
                    angle_degree = np.degrees(angle)
                    print("x=", locationxyz[0] , "y=",locationxyz[1], "yaw=" , angle_degree)
                    pose = Pose()
                    pose.position.x = locationxyz[0]
                    pose.position.y = locationxyz[1]
                    pose.position.z = 0.275
                    pose.orientation.w = angle + (0*math.pi/180)
                    self.pub.publish(pose)
                    self.counter_aruco = 0

            else:
                current_frame_with_marker_outlines = current_frame_gray

            # Publish the camera frame
            if (SHOULD_PUBLISH_CAMERA_IMAGES2):
                #rospy.loginfo("[ARUCO DETECTOR] Now publishing camera frame")
                self.image_publisher2.publish(self.cv_bridge.cv2_to_imgmsg(current_frame))

            # Save the camera frame if requested
            if (self.should_save_image2):
                # Increment the image counter
                self.save_image_counter2 += 1
                # Write the image to file
                temp_filename = SAVE_IMAGE_PATH2 + "aruco_image" + str(self.save_image_counter2) + ".jpg"
                cv2.imwrite(temp_filename,current_frame_with_marker_outlines)
                # Display the path to where the image was saved
                rospy.loginfo("[ARUCO DETECTOR] Save camera frame to: " + temp_filename)
                # Reset the flag to false
                self.should_save_image2 = False

            # Display the camera frame if requested
            if (SHOULD_SHOW_IMAGES2):
                #rospy.loginfo("[ARUCO DETECTOR] Now displaying camera frame")
                cv2.imshow("[ARUCO DETECTOR]", current_frame_with_marker_outlines)


    # Respond to subscriber receiving a message
    def requestSaveImageSubscriberCallback(self, msg):
        rospy.loginfo("[CAMERA CAPTURE] Request received to save the next image")
        # Set the flag for saving an image
        self.should_save_image = True

    # Respond to subscriber receiving a message
    def requestSaveImageSubscriberCallback2(self, msg):
        rospy.loginfo("[ARUCO DETECTOR] Request received to save the next image")
        # Set the flag for saving an image to true
        self.should_save_image2 = True       



if __name__ == '__main__':
    # Initialise the node
    global node_name
    node_name = "plant_capture"
    rospy.init_node(node_name)
    plant_capture_object = CameraCapture()
    # Spin as a single-threaded node
    rospy.spin()
    while not rospy.is_shutdown():
        plant_capture_object.timerCallbackForCameraRead
        rospy.sleep()
    # Release the camera
    plant_capture_object.cam.release()
    # Close any OpenCV windows
    cv2.destroyAllWindows()
