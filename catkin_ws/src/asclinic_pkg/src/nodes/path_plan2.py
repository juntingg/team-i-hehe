#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from scipy.interpolate import CubicSpline
import rospy
from geometry_msgs.msg import Pose
import matplotlib.pyplot as plt
from std_msgs.msg import Float32
from std_msgs.msg import Time
import math

#firstly orientation then go forward



def path_points(indices):
    # the matrix of available vertices 
    V = [[0, 0],
    [1.835+0.5, 0.81],
    [5.17 ,2.96],
    [8.648, 1.126],
    [0.5, 2.8],
    [0.5, 3.5],
    [0.5, 5],
    [9.5, 2.6],
    [9.5, 4.26],
    [3, 4.4375],
    #[3.5, 4.4375],
    [3, 5.8],
    #[3.5, 6.3],
    [1.87, 6.89],
    [1.13, 8.44],
    [1.742, 9.14],
    [5.2, 8.29],
    [7.615, 5.58],
    [7.29, 8.94],
    [9.037, 8.155],
    [1.726, 10.93],
    [5.2, 11.68],
    [7.695, 11],
    [9.13, 11.33],
    [0.6, 13],
    [0.6, 15],
    [9.48, 12.25],
    [9.48, 14.1],
    [1.62, 16.36],
    [5.24, 14.63],
    [8.53, 14.635],
    [8.719, 18.2],
    [5.35, 18.042],
    [1.467, 18.48],
    [1.792, 19.6],
    [8.608, 19.6],
    ]

    PATH_POINTS = [[0,0]]
    for index in indices:
        #print(index)
        PATH_POINTS.append(V[index])

    #print(PATH_POINTS)
    #print(PATH_POINTS[1])
    #print(PATH_POINTS[1][0])
    return PATH_POINTS[1:]  # moving the first point[0,0]

def calculate_distance(x1,y1,x2,y2):
    #(x1,y1) location (x2,y2) path points
    distance = math.sqrt((x2 - x1)**2 + (y2 - y1)**2)
    return distance

def find_shortest_points(path_points, location):
    shortest_distance = float('inf')
    second_shortest_distance = float('inf')
    shortest_point = None
    second_shortest_point = None
    
    for point in path_points:
        distance = calculate_distance(point[0],point[1], location[0],location[1])
        if distance < shortest_distance:
            # 当前距离比最短距离更短
            second_shortest_distance = shortest_distance
            second_shortest_point = shortest_point
            shortest_distance = distance
            shortest_point = point
        elif distance < second_shortest_distance:
            # 当前距离介于最短距离和第二短距离之间
            second_shortest_distance = distance
            second_shortest_point = point
    
    return shortest_point, second_shortest_point

# def find_vel_path_ind(x,y):
#     #fixed path
#     #path_points = [[1.835, 0.81], [4, 2.38], [3.5, 4.4375], [3.5, 6.3], [5.2, 8.29], [5.2, 11.68], [5.24, 14.63],[8.53, 14.635], [8.719, 18.2]]
#     path_points = [[1.835, 0.81], [5.17 ,2.96], [3.5, 4.4375], [3.5, 6.3], [5.2, 8.29], [5.2, 11.68], [5.24, 14.63],[8.53, 14.635], [8.719, 18.2]]
#     location = (x,y)
#     shortest_point, second_shortest_point = find_shortest_points(path_points,location)
#     if (shortest_point == path_points[0] and second_shortest_point == path_points[1] ) or (shortest_point == path_points[1] and second_shortest_point == path_points[0] ):
#         path_index = 1
#     elif (shortest_point == path_points[1] and second_shortest_point == path_points[2] ) or (shortest_point == path_points[2] and second_shortest_point == path_points[1] ):
#         path_index = 2
#     elif (shortest_point == path_points[2] and second_shortest_point == path_points[3] ) or (shortest_point == path_points[3] and second_shortest_point == path_points[2] ):
#         path_index = 3
#     elif   (shortest_point == path_points[3] and second_shortest_point == path_points[4] ) or (shortest_point == path_points[4] and second_shortest_point == path_points[3] ):
#         path_index = 4
#     elif (shortest_point == path_points[4] and second_shortest_point == path_points[5] ) or (shortest_point == path_points[5] and second_shortest_point == path_points[4] ):
#         path_index = 5
#     elif (shortest_point == path_points[5] and second_shortest_point == path_points[6] ) or (shortest_point == path_points[6] and second_shortest_point == path_points[5] ):
#         path_index = 6
#     elif (shortest_point == path_points[6] and second_shortest_point == path_points[7] ) or (shortest_point == path_points[7] and second_shortest_point == path_points[6] ):
#         path_index = 7
#     elif (shortest_point == path_points[7] and second_shortest_point == path_points[8] ) or (shortest_point == path_points[8] and second_shortest_point == path_points[7] ):
#         path_index = 8
#     else:
#         path_index = 1

#     if path_index == 1 or path_index ==4 or path_index ==7:
#         velocity = 0.2
#     elif path_index == 2 or path_index==3 or path_index ==5 or path_index ==6 or path_index == 8:
#         velocity = 0.2
#     else:
#         velocity = 0.2


#     return path_index

def find_ref_angle(index):

    #known path

    indices = [1,2,9,10,14]
    #indices = [1,2,9,10,14,19,27,28,29]
    PATH_POINTS = path_points(indices)
    # rospy.loginfo("available path points:", PATH_POINTS)
    # print("path_points:",PATH_POINTS)
    #extract x, y from path_points
    # x = []
    # for row in PATH_POINTS:
    #     x.append(row[0])
    # y = []
    # for row in PATH_POINTS:
    #     y.append(row[1])
    # print("x:",x)
    # print("y:",y)

    #calculate the reference angle
    ref_phas = []
    for i in range(len(PATH_POINTS) - 1):
        x1, y1 = PATH_POINTS[i]
        x2, y2 = PATH_POINTS[i+1]
        ref_pha = math.atan2(y2-y1, x2-x1) * (180/math.pi)
        ref_phas.append(ref_pha)
    #print("ref_phas:",ref_phas)

    ref_angle = ref_phas[index-1]

    return ref_angle

path_ind = 1
stop_com = 0

def vel_angle_callback(msg):
   
    #time -> predicted xy
    # current_time = rospy.Time.now()
    # run_time = (current_time - start_time).to_sec()
    # rospy.loginfo("Running time: %s",run_time)

    #x_pre,y_pre = trajectory_generation(cs_x,cs_y,run_time)

    #real xy -> velocity 
    # rospy.loginfo("[Path planning] Received real position x: %f, y: %f", msg.position.x, msg.position.y)
    local_x = msg.position.x
    local_y = msg.position.y
    local_w = msg.orientation.w
    # rospy.loginfo("[Path planning] Received real position x: %f, y: %f", local_x, local_y)

    # try: 
    #     path_ind
    # except NameError:
    #     path_ind = 1
    global path_ind
    global stop_com
    
    ref_angle = find_ref_angle(path_ind)

    points = [[1.835+0.5, 0.81], [5.17 ,2.96], [3, 4.4375], [3, 5.8], [5.2, 8.29]]
    #points = [[1.835, 0.81], [5.17 ,2.96], [3.5, 4.4375], [3.5, 6.3], [5.2, 8.29], [5.2, 11.68], [5.24, 14.63],[8.53, 14.635], [8.719, 18.2]]
    #if calculate_distance(local_x,local_y,points[path_ind][0],points[path_ind][1]) < 0.4:
    # if abs(local_x - points[path_ind][0]) < 0.4 and abs(local_y - points[path_ind][1])< 0.4:
    #     path_ind += 1 
    #     ref_angle = find_ref_angle(path_ind) #degree
    #rospy.loginfo("[Path planning]: ref_angle: %f, path_index: %d", ref_angle,path_ind)
    if path_ind == 1:
        if abs(local_y - points[path_ind][1]) < 0.4  and local_x > 5.1:
            path_ind = 2
            ref_angle = find_ref_angle(path_ind)
            stop_com = 0
    if path_ind == 2:
        if abs(local_y - points[path_ind][1]) < 0.2  and local_x < 3.05:
            path_ind = 3
            ref_angle = find_ref_angle(path_ind)
            stop_com = 0
    if path_ind == 3:
        if local_y  > 5.7:
            path_ind = 4
            ref_angle = find_ref_angle(path_ind)
            stop_com = 0
    if path_ind == 4 :
        if local_y > 6.5 and local_x > 5.5:
            stop_com = 1



    if stop_com == 0:
        if abs(ref_angle - local_w) > 10:
            velocity = 0
            angle_velocity = (ref_angle- local_w)*2
            if abs(angle_velocity) < 30:
                angle_velocity = math.copysign(1,angle_velocity) * 30
            if abs(angle_velocity) > 60:
                angle_velocity = math.copysign(1,angle_velocity) * 55
        elif path_ind <= 4:
            velocity = 0.15
            angle_velocity = 0
    else:
        velocity = 0
        angle_velocity = 0
        





    # # predicted and real x,y  -> angle
    # angle = find_angle(local_x,local_y,x_pre,y_pre)
    # #predicted points
    # point_pre_msg = Pose()
    # point_pre_msg.position.x = x_pre
    # point_pre_msg.position.y = y_pre
 
    # rospy.loginfo("Publishing predicted x: %f,predicted y: %f", x_pre, y_pre)
    # point_pub.publish(point_pre_msg)


    #line_velocity
    line_vel = Float32()
    line_vel.data = velocity
    # rospy.loginfo("Publishing calculated velocity: %f",pre_vel.data)
    line_vel_pub.publish(line_vel)

    #angle_velocity
    angle_vel = Float32()
    angle_vel.data = angle_velocity
    ang_vel_pub.publish(angle_vel)
    
    #reference angle velocity
    angle_ref = Float32()
    angle_ref.data = ref_angle
    ang_ref_pub.publish(angle_ref)

    #angle
    # pre_angle_msg = Float32()
    # pre_angle_msg.data = angle
    # rospy.loginfo("Publishing calculated angle: %f",pre_angle_msg.data)
    # angle_pub.publish(pre_angle_msg)

    #rospy.loginfo("[Path planning] current pose : %f, %f, %f ; line_vel : %f;ang_vel : %f; path index : %d;",local_x, local_y, local_w, velocity,angle_velocity,path_ind)
    #rospy.loginfo("[Path planning] ref_angle: %f",ref_angle)




if __name__ == "__main__":
    

    rospy.init_node("path_node2")
    #start_time = rospy.Time.now()


    #subscribers and publishers
    #time -> predicted position xy
    # point_pub = rospy.Publisher("path_points_topic",Pose,queue_size=10)
    #location -> velocity and angle
    line_vel_pub = rospy.Publisher("line_vel_topic",Float32,queue_size=10)
    ang_vel_pub = rospy.Publisher("angle_vel_topic",Float32,queue_size=100)
    ang_ref_pub = rospy.Publisher("angle_ref_topic",Float32,queue_size=100)
    local_sub = rospy.Subscriber("odometry",Pose,vel_angle_callback,queue_size=10)  
    rospy.spin()

    rate = rospy.Rate(100)

    while not rospy.is_shutdown():
        
        vel_angle_callback

        rate.sleep()


