
// Include some useful libraries
#include <cmath>
#include <stdlib.h>

// Include the ROS libraries
#include "ros/ros.h"
#include <ros/package.h>

// Include the standard message types
#include "std_msgs/Int32.h"
#include "std_msgs/Float32.h"
#include <std_msgs/String.h>
#include "asclinic_pkg/LeftRightInt32.h"
#include "asclinic_pkg/LeftRightFloat32.h"
#include "geometry_msgs/PoseStamped.h"

#include <vector>

// Publisher variable:
ros::Publisher odometry_control_publisher;

// Timer variable used for recurrent publishing:
ros::Timer m_timer_for_publishing;

float x_pose;
float y_pose;
float phi_pose;

float x_pose_target;
float y_pose_target;
float phi_pose_target;

float car_speed_target;
float car_speed;
float car_omega;

float m_delta_t_for_publishing_counts = 0.02;
float elapsed_time_in_seconds = 0.02;

float target_pwm_left = 0;
float target_pwm_right = 0;
float theta_left;
float theta_right;

float b = 0.215/2;
float r = 0.07;

float alpha = 0.95;
float prevOutput_left = 0.0;
float prevOutput_right = 0.0;

float a_1 = -0.00688;
float a_2 = -0.00719;

float b_1 = 1.375;
float b_2 = 1.433;

float c_1 = -6.56;
float c_2 = -6.401;

double integral_speed = 0;
double integral_omega = 0;

int windowSize = 3;
std::vector<float> input_phi = {0,0,0};
std::vector<float> weights = {0.2, 0.2, 0.6};

float prevOutput_phi_1;
float prevOutput_phi_2;
float prevOutput_phi_3;

float pre_phi = 20;

void odometryDataCallback(const geometry_msgs::Pose& pose_odometry)
{
	x_pose = pose_odometry.position.x;
	y_pose = pose_odometry.position.y;	
	phi_pose = pose_odometry.orientation.w;
}

// void pathDataCallback(const geometry_msgs::Pose& path_pose)
// {
// 	x_pose_target = path_pose.position.x;
// 	y_pose_target = path_pose.position.y;
// 	// phi_pose_target = path_pose.orientation.w;
// }

void velDataCallback(const std_msgs::Float32& vel)
{
	car_speed_target = vel.data;
}

void angleDataCallback(const std_msgs::Float32& angle)
{	
	phi_pose_target = angle.data;
}                          

float lowPassFilter(float prevOutput, float input) 
{
    float output = alpha * input + (1 - alpha) * prevOutput;
    return output;
}

float PI_omega(float target_omega, float current_omega, double integral)
{
	float error;
	error = target_omega - current_omega;
	const double Ts = 0.02; // Sampling time
	double Kp = 2; // Proportional gain
    double Ki = 0.7; // Integral gain
    integral += error * Ts;
    double u = Kp * error + Ki * integral;
    return u;
}

float PI_speed(float target_speed, float current_speed, double integral)
{
	float error;
	error = target_speed - current_speed;
	const double Ts = 0.02; // Sampling time
	double Kp = -0.5; // Proportional gain
    double Ki = 0.7; // Integral gain
    integral += error * Ts;
    double u = Kp * error + Ki * integral;
    return u;
}

float wmaFilter(const std::vector<float>& input, const std::vector<float>& weights, int windowSize) 
{
    float sumWeights = 0.0;
    float sumValues = 0.0;

    for (int i = 0; i < windowSize; i++) 
	{
        sumValues += weights[i] * input[i];
        sumWeights += weights[i];
    }

    return sumValues / sumWeights;
}

void timerCallbackForPublishing(const ros::TimerEvent&)
{

	elapsed_time_in_seconds += m_delta_t_for_publishing_counts;

	prevOutput_phi_1 = prevOutput_phi_2;
	prevOutput_phi_2 = prevOutput_phi_3;
	prevOutput_phi_3 = phi_pose_target;

	input_phi = {prevOutput_phi_1,prevOutput_phi_2,prevOutput_phi_3};

	phi_pose_target = wmaFilter(input_phi, weights, windowSize);



	// car_speed = 2 * ((x_pose_target - x_pose)*cos(phi_pose*M_PI/180)+(y_pose_target - y_pose)*sin(phi_pose*M_PI/180));
    car_omega = phi_pose_target;

    // car_speed = PI_speed(car_speed_target, car_speed, integral_speed);
    // car_omega = PI_omega(2 * phi_pose_target, phi_pose, integral_omega);

	car_speed = car_speed_target;

	// ROS_INFO_STREAM("[Odometry Control] phi target : " << phi_pose_target << " phi pose: " << phi_pose);
	// ROS_INFO_STREAM("[Odometry Control] pose from Odometry : " << x_pose << " , " << y_pose << " , " << phi_pose << "  car speed : " << car_speed);
	// ROS_INFO_STREAM("[odometry control] Car Speed : "<< car_speed << " Car Omega : " << car_omega);

	theta_left = ((car_speed - (b)*car_omega*M_PI/180)/r) * 90/(2 * M_PI);
	theta_right =  ((car_speed + (b)*car_omega*M_PI/180)/r) * 90/(2 * M_PI);

	// ROS_INFO_STREAM("[Odometry Control] left theta : " << theta_left);
	// ROS_INFO_STREAM("[Odometry Control] right theta : " << theta_right);

	theta_left = lowPassFilter(prevOutput_left, theta_left);
	theta_right = lowPassFilter(prevOutput_right, theta_right);

	if (theta_left > 0)
		target_pwm_left = (-b_1+sqrt(b_1*b_1 - 4*a_1*(c_1-theta_left)))/(2*a_1);
	else
		target_pwm_left = - (-b_1+sqrt(b_1*b_1 - 4*a_1*(c_1+theta_left)))/(2*a_1);

	if (theta_right > 0)
		target_pwm_right = (-b_2+sqrt(b_2*b_2 - 4*a_2*(c_2-theta_right)))/(2*a_2);
	else 
		target_pwm_right = - (-b_2+sqrt(b_2*b_2 - 4*a_2*(c_2+theta_right)))/(2*a_2);

	// ROS_INFO_STREAM("[Odometry Control] left PWM : " << target_pwm_left);
	// ROS_INFO_STREAM("[Odometry Control] right PWM : " << target_pwm_right);

	if (theta_left >= 62)
		{
			target_pwm_left = 80;
		}

	if (theta_right >= 62)
		{
			target_pwm_right = 80;
		}

	
	if (theta_left <= -62)
		{
			target_pwm_left = -80;
		}

	if (theta_right <= -62)
		{
			target_pwm_right = -80;
		}

	
	// publish the target speed and theta angle
	asclinic_pkg::LeftRightFloat32 target_msg;
	target_msg.left  = target_pwm_left;
	target_msg.right = target_pwm_right;
	odometry_control_publisher.publish(target_msg);
}

int main(int argc, char* argv[])
{

	ros::init(argc, argv, "odometry_control");
	ros::NodeHandle nodeHandle("~");

	std::string ns_for_group = ros::this_node::getNamespace();
	ros::NodeHandle nh_for_group(ns_for_group);

	odometry_control_publisher = nh_for_group.advertise<asclinic_pkg::LeftRightFloat32>("set_target_pwm", 2, false);

	ros::Subscriber odometry_subscriber = nh_for_group.subscribe("odometry", 1, odometryDataCallback);

	ros::Subscriber line_vel_subscriber = nh_for_group.subscribe("line_vel_topic", 1, velDataCallback);
	ros::Subscriber angle_vel_subscriber = nh_for_group.subscribe("angle_vel_topic", 1, angleDataCallback);
	// ros::Subscriber angle_vel_ref_subscriber = nh_for_group.subscribe("angle_vel_ref_topic", 2, pathDataCallback);


	// ros::Subscriber path_planning_subscriber = nh_for_group.subscribe("path_points_topic", 1, pathDataCallback);
	// ros::Subscriber vel_subscriber = nh_for_group.subscribe("pre_vel_topic", 1, velDataCallback);
	// ros::Subscriber angle_subscriber = nh_for_group.subscribe("pre_angle_topic", 1, angleDataCallback);

	// Initialise a timer
	m_timer_for_publishing = nodeHandle.createTimer(ros::Duration(m_delta_t_for_publishing_counts), timerCallbackForPublishing, false);

	ros::spin();

	return 0;
}
